# Projet Bot Discord Scraping du site Oryx

Ce projet Discord utilise une combinaison de web scraping et de manipulation de messages pour afficher des informations spécifiques à partir d'un site Web et répondre à certaines commandes.

## Description

Ce bot Discord récupère des données à partir d'une source en ligne spécifique et fournit des fonctionnalités permettant aux utilisateurs de visualiser ces données à l'intérieur de leur serveur Discord.

## Fonctionnalités

- Affichage des phrases avec le nombre de destructions/captures et de dommages/abandons par modèle.
- Affichage du total de destructions/captures par catégorie.

## Commandes

- `!help` : Affiche la liste des commandes disponibles.
- `!all_models` : Affiche les phrases avec le nombre de destructions/captures et de dommages/abandons par modèle.
- `!all_categories` : Affiche le total de destructions/captures par catégorie.

## Configuration

- Assurez-vous d'installer les dépendances nécessaires répertoriées dans le fichier `data.py`.
- Ajoutez votre jeton Discord dans le fichier `bot.py` pour exécuter le bot.

## Exécution

- Faire la commande `source bot-env/bin/activate`.
- Installer les dépendances discord, requests et bs4.
- Exécutez le fichier `bot.py` pour démarrer le bot Discord.

## Dépendances

- Discord.py
- Requests
- Beautiful Soup (bs4)

## Ressenti

J'ai découvert le scraping et plusieurs façons de localiser des données dans un site et plusieurs façons de trier les données ce qui a été
très enrichissant mais aussi assez complexe car le site dont j'ai scrapé n'était pas constant dans les balises qu'ils utilisais pour les mêmes données
et dans la structure du site. Dếcouvrir comment fonctionne un bot discord était assez satisfaisant et j'ai aimé découvrir cela.