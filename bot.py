import discord
import asyncio
import requests
import re
from bs4 import BeautifulSoup
from data import phrases_to_include

class MyClient(discord.Client):
    async def on_ready(self):
        # Message lorsque le bot est connecté et prêt
        print(f'Logged on as {self.user}!')

    async def on_message(self, message):
        # Gestion des messages reçus
        if message.content.startswith('!help'):
            await self.display_help(message)
        elif message.content.startswith('!all_models'):
            await self.display_phrases_with_destroyed_count(message)
        elif message.content.startswith('!all_categories'):
            await self.display_category_totals(message)
        print(f'Message from {message.author}: {message.content}')

    async def display_help(self, message):
        # Affichage de l'aide avec les commandes disponibles
        help_message = "List of available commands:\n" \
                       "`!all_models` : Displays phrases with the number of destroyed/captured and damaged/abandoned by model.\n" \
                       "`!all_categories` : Displays total destroyed/captured by category.\n"
        await message.channel.send(help_message)

    async def display_phrases_with_destroyed_count(self, message):
        # Affichage des phrases avec le nombre de destructions/captures et de dommages/abandons par modèle
        phrases_with_destroyed_count = self.get_phrases_with_destroyed_count(phrases_to_include)
        if phrases_with_destroyed_count:
            for category, chunk in phrases_with_destroyed_count.items():
                formatted_messages = [f"**{category}**"]
                for phrase, count in chunk:
                    num_part = re.sub(r'\D', '', phrase.split(' ', 1)[0].strip())
                    if num_part.isdigit():
                        da = int(num_part) - count
                        premodel = phrase.split(' ', 1)[1].strip()
                        model = premodel.split(':')[0].strip()
                        formatted_messages.append(f"{model} - Destroyed/Captured : {count} - Damaged/Abandoned : {da}")
                    else:
                        formatted_messages.append("Invalid format: couldn't extract a valid number from the phrase.")
                await message.channel.send("\n".join(formatted_messages))
        else:
            await message.channel.send("No phrases found after images.")

    async def display_category_totals(self, message):
        # Affichage du total de destructions/captures par catégorie
        phrases_with_destroyed_count = self.get_phrases_with_destroyed_count(phrases_to_include)
        if phrases_with_destroyed_count:
            category_totals = {}
            for category, chunk in phrases_with_destroyed_count.items():
                total_count = sum(count for _, count in chunk)
                category_totals[category] = total_count

            formatted_messages = ["**Total by category**"]
            for category, total_count in category_totals.items():
                formatted_messages.append(f"{category} - Total Destroyed/Captured : {total_count}")
            await message.channel.send("\n".join(formatted_messages))
        else:
            await message.channel.send("No phrases found after images.")

    def get_phrases_with_destroyed_count(self, phrases_to_include):
        # Récupération des phrases avec le nombre de destructions/captures
        url = "https://www.oryxspioenkop.com/2022/02/attack-on-europe-documenting-ukrainian.html"
        response = requests.get(url)
        if response.status_code == 200:
            soup = BeautifulSoup(response.content, 'html.parser')
            phrases_with_destroyed_count = {}
            current_category = None
            for li in soup.find_all('li'):
                img = li.find('img', class_='thumbborder')
                if img:
                    text = li.get_text(separator='|', strip=True)
                    text_before_destroyed = text.split('destroyed')[0]
                    destroyed_count = text.count('destroyed')
                    captured_count = text.count('captured')
                    total_count = destroyed_count + captured_count
                    phrase = text_before_destroyed.replace('|', ' ')
                    category_span = li.find_previous('span', class_='mw-headline')
                    if category_span:
                        current_category = category_span.text.strip()
                        if any(phrase_to_include in phrase for phrase_to_include in phrases_to_include):
                            phrases_with_destroyed_count.setdefault(current_category, []).append((phrase, total_count))
            return phrases_with_destroyed_count
        else:
            return {}

intents = discord.Intents.default()
intents.message_content = True

client = MyClient(intents=intents)
client.run('MTIwMTgwMTM2NTUxNzc3MDc3Mg.GKgbn6.AN992SlFJPvpMwOVVmDsf5OTs4BNnrh7cTlx5Y')
